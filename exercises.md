# Exercises

## Be the compiler

### A

Code will error-out, when `x` is four, as `hobbits[4]` will
be out of the array's bounds.

### B

`$firemen[firemanNo]` should be enclosed in curly-brackets.
